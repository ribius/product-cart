<?php

namespace app\services;

use yii\base\Exception;
use yii\base\InvalidArgumentException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use yii\helpers\Json;

class CleverpaymentRestClient
{
  const API_URL = 'https://rest.cleverpayment.ru';
  const API_VERSION = 'v1';
  private $access_token;

  public function __construct($access_token)
  {
    $this->access_token = $access_token;
  }

  /**
   * @param $amount
   * @param $org_code
   * @param $products @array
   * [
   *  [
   *    'product_id' => '',
   *    'quantity' => '',
   *    'cost' => '',
   *  ]
   * ]
   * @return mixed
   * @throws Exception
   */
  public function createOrder($amount, $org_code, $products)
  {
    $data = [
      'amount' => $amount,
      'organization_code' => $org_code,
      'items' => $products,
    ];

    $url = self::API_URL . '/' . self::API_VERSION . '/order/create';
    return $this->postRequest($url, $data);
  }

  public function createPayment($order_id, $payment_service_id, $ammount)
  {
    $data = [
      'order_id' => $order_id,
      'service_id' => $payment_service_id,
      'amount' => round($ammount, 2),
    ];
    $url = self::API_URL . '/' . self::API_VERSION . '/payment/create';
    return $this->postRequest($url, $data);
  }

  public function getOrder($order_id)
  {
    $url = self::API_URL . '/' . self::API_VERSION . '/order/view/' . $order_id;
    return $this->getRequest($url);
  }

  public function getPayLink($url_token, $payment_type, $email = null, $phone = null)
  {
    $url = self::API_URL . '/' . self::API_VERSION . '/payment-page/index';
    $data = [
      'token' => $url_token,
      'type_id' => $payment_type,
    ];
    if($email) $data['email'] = $email;
    if($phone) $data['phone'] = $phone;
    return $this->postRequest($url, $data);
  }

  public function getPaymentTypeList($payment_service_id)
  {
    $url = self::API_URL . '/' . self::API_VERSION . "/payment-service/view/{$payment_service_id}";
    return $this->getRequest($url);
    return false;
  }

  protected function postRequest($url, $data)
  {
    $client = new Client(['http_errors' => false]);
    try {
      $response = $client->post($url, [
        'form_params' => $data,
        'headers' => [
          'Authorization' => "Bearer {$this->access_token}",
        ],
      ]);
      if($response->getStatusCode() == 200) {
        $body = Json::decode($response->getBody());
        return $body;
      }elseif($response->getStatusCode() >= 400) {
        $body = Json::decode($response->getBody());
        throw new InvalidArgumentException($body['message']);
      }
    } catch (BadResponseException $e) {
      throw new Exception($e->getMessage());
    }
  }

  protected function getRequest($url)
  {
    $client = new Client(['http_errors' => false]);
    try {
      $response = $client->get($url, [
        'headers' => [
          'Authorization' => "Bearer {$this->access_token}",
        ],
      ]);
      if($response->getStatusCode() == 200) {
        $body = Json::decode($response->getBody());
        return $body;
      }elseif($response->getStatusCode() >= 400) {
        $body = Json::decode($response->getBody());
        throw new InvalidArgumentException($body['message']);
      }
    } catch (BadResponseException $e) {
      throw new Exception($e->getMessage());
    }
  }
}