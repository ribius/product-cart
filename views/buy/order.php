<?php

use yii\grid\GridView;
use yii\widgets\ListView;
use app\activeRecords\CartItem as CartItemActiveRecord;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $buyForm \app\models\BuyForm */
?>

<?php if(Yii::$app->session->hasFlash('error')): ?>
  <div class="alert alert-danger">
    <?php print Yii::$app->session->getFlash('cartNotFound'); ?>
  </div>
<?php endif; ?>
<?php if(Yii::$app->session->hasFlash('createError')): ?>
  <div class="alert alert-danger">
    <?php print Yii::$app->session->getFlash('createError'); ?>
  </div>
<?php endif; ?>
