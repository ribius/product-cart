<?php

/** @var $this \yii\web\View */

$this->registerJs(<<<JS
  (function($) {
    $.ajax({
      url: window.location,
      method: 'GET',
      dataType: 'json',
      success: function(data) {
        $('.preloader').hide();
        if(data.redirect_url) {
        	window.location = data.redirect_url;
        }
      },
      error: function(data) {
      	$('.preloader').hide();
      	$('.order-wrap .alert').html(data.responseJSON.message).addClass('alert-danger');
        console.log(data);
      }
    });
  })(jQuery)
JS
, $this::POS_READY);

?>

<div class="row">
  <div class="col-md-10 col-md-offset-1 order-wrap">
    <div class="alert"></div>
  </div>
</div>
