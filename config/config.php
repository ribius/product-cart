<?php

$db = require __DIR__ . '/db.php';
$db2 = require __DIR__ . '/db2.php';

$config = [
  'id' => 'cpcart',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
  ],
  'components' => [
    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => '0Ni7_FvQGIc728KJaHnACoATh3fZmDIs',
      'parsers' => [
        'application/json' => 'yii\web\JsonParser',
      ]
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
      'defaultDuration' => 60*60*2,
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => false,
      'enableSession' => false,
      'loginUrl' => null,
    ],
    'errorHandler' => [
      'errorAction' => 'buy/error',
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
            'class' => 'yii\log\FileTarget',
            'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db' => $db,
    'db2' => $db2,
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
        'order/<id:.*>' => 'buy/product',
        'agree' => 'buy/agree',
      ],
    ],
  ],
  'params' => [
    'payment_url' => 'http://clpt-pay.loc/payment',
  ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
