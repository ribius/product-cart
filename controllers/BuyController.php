<?php

namespace app\controllers;

use app\models\Cart;
use Yii;
use app\forms\BuyForm;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BuyController extends Controller
{

  public function actions()
  {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }

  public function actionProduct($id)
  {
    if(Yii::$app->request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      try {
        $cart = Cart::findById($id);
        /* @var $form BuyForm */
        $form = new BuyForm($cart);
        $form->createOrder();
        $form->createPayment();
        $payment = $form->getPayment();
        return ['redirect_url' => $payment['url']];
      }catch (NotFoundHttpException $e) {
        throw new NotFoundHttpException($e->getMessage());
      }catch(InvalidArgumentException $e) {
        throw new BadRequestHttpException($e->getMessage());
      }catch (\Exception $e) {
        throw new BadRequestHttpException($e->getMessage());
      }
      return $this->renderPartial('order');
    }else {
      return $this->render('clear');
    }
  }

}
