<?php

namespace app\controllers;

use app\forms\CartForm;
use app\forms\CartItemCreateForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidCallException;
use yii\helpers\VarDumper;
use yii\web\Controller;
use app\models\Cart;
use app\models\CartItem;
use app\activeRecords\CartItem as CartItemActiveRecord;
use yii\filters\Cors;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use yii\web\Response;

class CartController extends Controller
{
  public function behaviors()
  {
    return [
      'corsFilter' => [
        'class' => Cors::class,
        'cors' => [
          'Origin' => ['*'],
          'Access-Control-Request-Method' => ['POST'],
          'Access-Control-Request-Headers' => ['*'],
        ]
      ],
    ];
  }

  public function beforeAction($action)
  {
    $this->enableCsrfValidation = false;
    Yii::$app->getResponse()->format = Response::FORMAT_JSON;

    return parent::beforeAction($action);
  }

  public function actionAdd()
  {
    $cartItem = new CartItemCreateForm();
    $cartItem->setAttributes(Yii::$app->request->post());
    if(!$cartItem->validate()) {
      throw new InvalidArgumentException(implode(PHP_EOL, $cartItem->getFirstErrors()));
    }
    if(Yii::$app->request->post('cart_id') == null) {
      $cart = Cart::create();
    }else {
      $cart = Cart::findById(Yii::$app->request->post('cart_id'));
    }
    if($cartItem->create($cart->id)) {
      return self::output(Cart::findById($cart->id));
    }

    throw new ServerErrorHttpException('Ошибка сервера cart');
  }

  public function actionItemDecrement()
  {
    $cartItem = CartItemActiveRecord::findById(Yii::$app->request->post('item_id'));
    $cartItem = new CartItem($cartItem);
    $cartItem->quantityDecrement();
    return self::output(Cart::findById($cartItem->cartid));
  }

  public function actionItemIncrement()
  {
    $cartItem = CartItemActiveRecord::findById(Yii::$app->request->post('item_id'));
    $cartItem = new CartItem($cartItem);
    $cartItem->quantityIncrement();
    return self::output(Cart::findById($cartItem->cartid));
  }

  public function actionItemDelete()
  {
    $cartItem = CartItemActiveRecord::findById(Yii::$app->request->post('item_id'));
    $cartItem = new CartItem($cartItem);
    $cartid = $cartItem->cartid;
    $cartItem->delete();
    return self::output(Cart::findById($cartid));
  }

  public function actionGet()
  {
    return self::output(Cart::findById(Yii::$app->request->post('cart_id')));
  }

  public function actionFlush()
  {
    $cart = Cart::findById(Yii::$app->request->post('cart_id'));
    try {
      $cart = $cart->flush();
      return self::output($cart);
    }catch (\Throwable $e) {
      Yii::error('Ошибка при очистки корзины: ' . implode(PHP_EOL, $e->getTrace()));
    }
  }

  protected function output(Cart $cart)
  {
    $output = [
      'cart_id' => $cart->id,
      'status' => $cart->status,
      'created' => $cart->created,
      'changed' => $cart->changed,
      'total_price' => $cart->totalprice,
    ];
    $items = $cart->items;
    if(count($items) > 0) {
      $output['items'] = [];
      foreach($items as $item) {
        $output['items'][] = [
          'id' => $item->id,
          'name' => $item->productname,
          'full_name' => $item->productfullname,
          'cart_id' => $item->cartid,
          'product_id' => $item->productid,
          'price' => $item->price,
          'total_price' => $item->totalprice,
          'quantity' => $item->quantity,
        ];
      }
    }

    return $output;
  }
}
