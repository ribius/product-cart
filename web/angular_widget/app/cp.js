requirejs.config({
  baseUrl: cpConfig.base_url,
  urlArgs: "v=" + (new Date()).getMonth() + '.' + (new Date()).getDay() + '.' + (new Date()).getSeconds(),
  paths:{
    'angular': 'angular_widget/app/cp-cart-bundle',
    'app': "angular_widget/app/app",
    'service': "angular_widget/app/dataService",
    'controller': 'angular_widget/app/controller'
  },
  shim: {
    'app': ['angular'],
    'controller': ['app'],
    'service': ['app']
  }
});
requirejs(["app", "controller", "service"], function() {

  main();

  function main() {
		addCss(cpConfig.base_url + '/' + cpConfig.app_path + '/' + cpConfig.bower_path + '/angular-material/angular-material.min.css');
    addCss(cpConfig.base_url + '/' + cpConfig.app_path + '/css/cp.css?v=0.4');
    var ngInclude = document.createElement('div');
    //ngView.setAttribute('ng-view', '');
    ngInclude.setAttribute('ng-include', "'" + cpConfig.base_url + '/' + cpConfig.app_path + '/views/init.html?v=1.0' + "'");
    document.body.appendChild(ngInclude);
    document.body.setAttribute('ng-controller', 'cartController');
    document.write = document._write;
    angular.element(document.getElementsByTagName('head')).append(angular.element('<base href="' + window.location.pathname + '" />'));
    angular.element(document).ready(function() {
      angular.bootstrap(document, ['cp']);
    });
  }

  function addCss(fileName) {
    var head = document.head;
    var link = document.createElement("link");

    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = fileName;

    head.appendChild(link);
  }

})();