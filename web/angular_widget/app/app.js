'use strict';

var cp = angular.module('cp', [
  'ngRoute',
  'ngCookies',
  'cgNotify',
  'ngAria',
  'ngMaterial'
])
.constant('baseUrl', cpConfig.base_url)
.constant('appPath', cpConfig.app_path)
.constant('pPrimary', cpConfig.pPrimary)
.constant('pAccent', cpConfig.pAccent)
.config(function($locationProvider, $routeProvider, $sceDelegateProvider, $mdThemingProvider) {
		$sceDelegateProvider.resourceUrlWhitelist(['self', cpConfig.base_url + '/**']);
		$mdThemingProvider.theme('default').primaryPalette(cpConfig.pPrimary).accentPalette(cpConfig.pAccent);
    /*$routeProvider
    .when('/', {
      templateUrl: cpConfig.base_url + '/' + cpConfig.app_path + '/views/init.html?v=1.0',
      controller: 'cartController'
    });*/
  //$locationProvider.hashPrefix('!');
  //$locationProvider.html5Mode(true);
    //$routeProvider.otherwise({redirectTo: '/'});
}).run(function(notify) {
	notify.config({
		duration:2000,
		templateUrl:cpConfig.base_url + '/' + cpConfig.app_path + '/views/notify.html'
	});
});

