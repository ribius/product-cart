cp.factory('dataService', function($http, $q, $cookies, notify){
		var actions_url = cpConfig.base_url + '/cart';
		var actionsUrls = {
			'cart_get': actions_url + '/' + 'get',
			'item_add': actions_url + '/' + 'add',
			'item_decrement': actions_url + '/' + 'item-decrement',
			'item_increment': actions_url + '/' + 'item-increment',
			'item_delete': actions_url + '/' + 'item-delete',
			'cart_flush': actions_url + '/' + 'flush'
		};

		httpPost = function(url, data, msg = null) {
			var deferred = $q.defer();
			$http.post(url, data).then (function success(response) {
				deferred.resolve(response.data);
				$cookies.putObject('cart', response.data);
				if(msg) {

					notify(msg);
				}
			}, function error(response) {
				deferred.reject(response.data)
				notify(response.data.message);
			});
			return deferred.promise;
		};

    return{
			getCart: function(){
				var cart = $cookies.getObject('cart');
				if(cart !== undefined) {
					return cart;
					//return httpPost(actionsUrls.cart_get, {'cart_id': cart.cart_id});
				}else {
					//notify("Корзина пуста");
				}
			},
			addItemToCart: function(id){
				var data = {
					product_id:id,
					quantity: 1
				};
				var cart = $cookies.getObject('cart');
				if(cart !== undefined) data.cart_id = cart.cart_id;
				return httpPost(actionsUrls.item_add, data, "Товар добавлен в корзину");
			},
			itemDecrement: function(item){
				if(item.quantity-1 < 1) {
					notify("Колличество не может быть меньше единицы");
				}else {
					return httpPost(actionsUrls.item_decrement, {'item_id': item.id});
				}
			},
			itemIncrement: function(item){
				return httpPost(actionsUrls.item_increment, {'item_id': item.id});
			},
			itemDelete: function(item){
				return httpPost(actionsUrls.item_delete, {'item_id': item.id}, "Товар удален из корзины");
			},
			cartFlush: function(cart_id){
				return httpPost(actionsUrls.cart_flush, {'cart_id': cart_id}, "Корзина очищенна");
			}
    }
});