cp.controller('cartController', ['$scope', 'dataService', '$routeParams', '$location', '$mdDialog',
  function cartController($scope, dataService, $routeParams, $location, $mdDialog) {
    $scope.cart = dataService.getCart();
    console.log($scope.cart);
		$scope.addToCart = function(ProductId) {
			$scope.preloaderShow = true;
			dataService.addItemToCart(ProductId).then(function (value) {
				$scope.preloaderShow = false;
        $scope.cart = value;
        console.log($scope.cart);
			}).catch(function(err) {
				$scope.preloaderShow = false;
			});
		};

		$scope.openCart = function(ev) {
			$mdDialog.show({
				title: 'Корзина',
				controller: cartView,
				templateUrl: cpConfig.base_url + '/' + cpConfig.app_path + '/views/view.html?v=0.2',
				parent: angular.element(document.body),
				clickOutsideToClose: true,
				locals: {
					cart: $scope.cart
				},
				//scope: $scope,
				bindToController: true,
				targetEvent: ev,
				openFrom: '.cp-cart',
				closeTo: '.cp-cart',
				onRemoving: function (event, removePromise) {
          $scope.cart = dataService.getCart();
				}
			});
		};

		function cartView($scope, $mdDialog, dataService, notify, cart){
			$scope.cart = cart;
			$scope.itemDecrement = function(item) {
				if(item.quantity-1 < 1) {
					notify("Колличество не может быть меньше единицы");
				}else {
					$scope.preloaderShow = true;
					dataService.itemDecrement(item).then(function (value) {
						$scope.preloaderShow = false;
						$scope.cart = value;
					}).catch(function (err) {
						$scope.preloaderShow = false;
					});
				}
			};
			$scope.itemIncrement = function(item) {
				$scope.preloaderShow = true;
				dataService.itemIncrement(item).then(function (value) {
					$scope.preloaderShow = false;
					$scope.cart = value;
				}).catch(function(err) {
					$scope.preloaderShow = false;
				});
			};
			$scope.itemDelete = function(item) {
				$scope.preloaderShow = true;
				dataService.itemDelete(item).then(function (value) {
					$scope.preloaderShow = false;
					$scope.cart = value;
				}).catch(function(err) {
					$scope.preloaderShow = false;
				});
			};
			$scope.cartFlush = function(cart_id) {
				$scope.preloaderShow = true;
				dataService.cartFlush(cart_id).then(function (value) {
					$scope.preloaderShow = false;
					$scope.cart = {};
					$mdDialog.hide();
					//$location.path('/');
				}).catch(function(err) {
					$scope.preloaderShow = false;
				});
			};
			$scope.cartClose = function() {
				$mdDialog.hide();
			};
			$scope.cartOrder = function (cart_id) {
				window.location.href = cpConfig.base_url + '/order/' + cart_id;
			};
		}
  }]
);