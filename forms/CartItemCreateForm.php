<?php

namespace app\forms;

use app\models\Cart;
use yii\base\Model;
use app\models\CartItem;
use app\activeRecords\CartItem as CartItemActiverecord;
use yii\helpers\VarDumper;

class CartItemCreateForm extends Model
{
  public $product_id;
  public $quantity;

  public function rules()
  {
    return [
      [['product_id', 'quantity'], 'required'],
      [['quantity'], 'integer'],
      [['product_id'], 'string'],
    ];
  }

  public function create($cart_id)
  {
    if(!$this->validate()) {
      return false;
    }
    if(($cartItem = CartItemActiverecord::findByProductAndCart($this->product_id, $cart_id))) {
      $cartItem = new CartItem($cartItem, []);
      return $cartItem->quantityIncrement();
    }
    $cartItem = new CartItem(new CartItemActiverecord(), []);
    return $cartItem->create($cart_id, $this->product_id, $this->quantity);
  }
}