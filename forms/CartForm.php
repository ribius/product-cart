<?php

namespace app\forms;

use yii\base\Model;
use app\activeRecords\Cart;

class CartForm extends Model
{

  public function attributeLabels()
  {
    return Cart::attributeLabels();
  }

  public function create()
  {
    if(!$this->validate()) {
      return null;
    }
    return Cart::create();
  }

}