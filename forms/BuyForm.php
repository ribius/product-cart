<?php

namespace app\forms;


use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use yii\helpers\Json;
use app\models\Cart;
use app\services\CleverpaymentRestClient;

class BuyForm extends Model
{
    protected $order;
    protected $payment;
    protected $cart;
    protected $cl_client;

    public function __construct(Cart $cart, array $config = [])
    {
        $this->cart = $cart;
        $this->cl_client = new CleverpaymentRestClient($this->cart->accesstoken);
        parent::__construct($config);
    }

    public function createOrder()
    {
      $products = [];
      foreach ($this->cart->items as $item) {
        $products[] = [
          'product_id' => $item->productproductid,
          'quantity' => $item->quantity,
          'cost' => $item->totalprice,
        ];
      }
      $this->order = $this->cl_client->createOrder($this->cart->totalprice, $this->cart->clientcode, $products);
    }

    public function createPayment()
    {
      $this->payment = $this->cl_client->createPayment($this->order['id'], $this->cart->paymentserviceid, $this->order['amount']);
    }

    public function getPayment()
    {
      return $this->payment;
    }
}
