<?php

namespace app\activeRecords;

use app\activeRecords\CCashRegister;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "g_product_reference".
 *
 * @property int $id
 * @property string $code
 * @property int $product_id
 * @property int $user_id
 * @property int $organization_id
 * @property int $payment_service_id
 * @property int $price
 */
class GProductReference extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'g_product_reference';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['code', 'product_id', 'user_id', 'organization_id', 'payment_service_id', 'price'], 'required'],
      [['product_id', 'user_id', 'organization_id', 'payment_service_id', 'price'], 'integer'],
      [['code'], 'string', 'max' => 20],
      [['code'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'code' => 'Code',
      'product_id' => 'Product ID',
      'user_id' => 'User ID',
      'organization_id' => 'Organization ID',
      'payment_service_id' => 'Payment Service ID',
      'price' => 'Price',
    ];
  }

  /**
   * @return null|object|\yii\db\Connection
   */
  public static function getDb() {
    return Yii::$app->get('db2'); // new database
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProduct()
  {
    return $this->hasOne(GProduct::class, ['id' => 'product_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser()
  {
    return $this->hasOne(UUser::class, ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCash_register()
  {
    return $this->hasOne(CCashRegister::class, ['id' => 'cash_register_id']);
  }
}
