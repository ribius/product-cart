<?php

namespace app\activeRecords;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "c_organization".
 *
 * @property int $id
 * @property string $name
 * @property string $client_code
 * @property int $client_id
 * @property int $created_at
 * @property string $address
 * @property string $payment_place
 * @property string $itin
 */
class COrganization extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'c_organization';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name', 'client_code', 'client_id', 'created_at'], 'required'],
      [['client_id', 'created_at'], 'integer'],
      [['name'], 'string', 'max' => 255],
      [['code'], 'string', 'max' => 20],
      [['address', 'payment_place'], 'string', 'max' => 1000],
      [['itin'], 'string', 'max' => 15],
      [['code'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'client_code' => 'Code',
      'client_id' => 'Client ID',
      'created_at' => 'Created At',
      'address' => 'Address',
      'payment_place' => 'Payment Place',
      'itin' => 'Itin',
    ];
  }

  /**
   * @return null|object|\yii\db\Connection
   */
  public static function getDb() {
    return Yii::$app->get('db2');
  }
}
