<?php

namespace app\activeRecords;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $status
 * @property int $created
 * @property int $changed
 */
class Cart extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'cart';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'status' => 'Status',
      'created' => 'Created',
      'changed' => 'Changed',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getItems()
  {
    return $this->hasMany(CartItem::class, ['cart_id' => 'id']);
  }

  /**
   * @return int
   */
  public function getTotalPrice()
  {
    $price = 0;
    $items = $this->items;
    if(count($items)) {
      foreach ($items as $item) {
        $price += $item->total_price;
      }
    }
    return $price;
  }
}
