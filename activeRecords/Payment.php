<?php

namespace app\activeRecords;

use Yii;
use app\activeRecords\PsPaymentService;

/**
 * This is the model class for table "o_payment".
 *
 * @property int $id
 * @property int $order_id
 * @property string $name
 * @property string $client_code
 * @property int $cash_register_id
 * @property int $payment_service_id
 * @property int $payment_type_id
 * @property int $amount
 * @property int $amount_paid
 * @property int $amount_refund
 * @property int $amount_system
 * @property int $amount_client
 * @property int $status_id
 * @property int $user_id
 * @property int $client_id
 * @property string $options
 * @property string $comment
 * @property string $payment_system_url
 * @property string $payment_system_tid
 * @property string $payment_system_status
 * @property string $payment_system_message
 * @property int $created_at
 * @property int $updated_at
 * @property int $date_paid
 * @property int $is_client_notified
 * @property string $url_token
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'o_payment';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'name', 'client_code', 'payment_type_id', 'amount', 'user_id', 'client_id', 'created_at', 'updated_at'], 'required'],
            [['order_id', 'cash_register_id', 'payment_service_id', 'payment_type_id', 'amount', 'amount_paid', 'amount_refund', 'amount_system', 'amount_client', 'user_id', 'client_id', 'created_at', 'updated_at', 'date_paid'], 'integer'],
            [['options'], 'string'],
            [['name'], 'string', 'max' => 1000],
            [['client_code', 'payment_system_tid', 'payment_system_status', 'url_token'], 'string', 'max' => 255],
            [['status_id'], 'string', 'max' => 4],
            [['comment'], 'string', 'max' => 3000],
            [['payment_system_url', 'payment_system_message'], 'string', 'max' => 2000],
            [['is_client_notified'], 'string', 'max' => 1],
            [['client_id', 'client_code'], 'unique', 'targetAttribute' => ['client_id', 'client_code']],
            [['url_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'client_code' => 'Client Code',
            'cash_register_id' => 'Cash Register ID',
            'payment_service_id' => 'Payment Service ID',
            'payment_type_id' => 'Payment Type ID',
            'amount' => 'Amount',
            'amount_paid' => 'Amount Paid',
            'amount_refund' => 'Amount Refund',
            'amount_system' => 'Amount System',
            'amount_client' => 'Amount Client',
            'status_id' => 'Status ID',
            'user_id' => 'User ID',
            'client_id' => 'Client ID',
            'options' => 'Options',
            'comment' => 'Comment',
            'payment_system_url' => 'Payment System Url',
            'payment_system_tid' => 'Payment System Tid',
            'payment_system_status' => 'Payment System Status',
            'payment_system_message' => 'Payment System Message',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'date_paid' => 'Date Paid',
            'is_client_notified' => 'Is Client Notified',
            'url_token' => 'Url Token',
        ];
    }

    public function getPaymentservice()
    {
      return $this->hasOne(PsPaymentService::class, ['id' => 'payment_service_id']);
    }
}
