<?php

namespace app\activeRecords;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "u_user".
 *
 * @property int $id
 * @property string $email
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email_confirm_token
 * @property string $admin_token
 * @property string $access_token
 * @property int $status_id
 * @property int $client_id
 * @property int $client_role_id
 * @property string $time_zone
 * @property int $created_at
 * @property int $updated_at
 */
class UUser extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'u_user';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['email', 'username', 'auth_key', 'password_hash', 'access_token', 'created_at', 'updated_at'], 'required'],
      [['client_id', 'created_at', 'updated_at'], 'integer'],
      [['email', 'username', 'password_hash', 'password_reset_token', 'email_confirm_token', 'admin_token', 'access_token', 'time_zone'], 'string', 'max' => 255],
      [['auth_key'], 'string', 'max' => 32],
      [['status_id', 'client_role_id'], 'string', 'max' => 4],
      [['email'], 'unique'],
      [['access_token'], 'unique'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'email' => 'Email',
      'username' => 'Username',
      'auth_key' => 'Auth Key',
      'password_hash' => 'Password Hash',
      'password_reset_token' => 'Password Reset Token',
      'email_confirm_token' => 'Email Confirm Token',
      'admin_token' => 'Admin Token',
      'access_token' => 'Access Token',
      'status_id' => 'Status ID',
      'client_id' => 'Client ID',
      'client_role_id' => 'Client Role ID',
      'time_zone' => 'Time Zone',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
    ];
  }

  public static function getDb() {
    return Yii::$app->get('db2');
  }
}
