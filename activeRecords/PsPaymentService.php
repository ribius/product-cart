<?php

namespace app\activeRecords;

use Yii;

/**
 * This is the model class for table "ps_payment_service".
 *
 * @property int $id
 * @property int $client_id
 * @property string $name
 * @property string $display_name
 * @property string $notify_url
 * @property string $payment_success_url
 * @property string $payment_error_url
 * @property int $is_default
 */
class PsPaymentService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ps_payment_service';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'name'], 'required'],
            [['client_id'], 'integer'],
            [['name', 'display_name'], 'string', 'max' => 255],
            [['notify_url', 'payment_success_url', 'payment_error_url'], 'string', 'max' => 1000],
            [['is_default'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'name' => 'Name',
            'display_name' => 'Display Name',
            'notify_url' => 'Notify Url',
            'payment_success_url' => 'Payment Success Url',
            'payment_error_url' => 'Payment Error Url',
            'is_default' => 'Is Default',
        ];
    }
}
