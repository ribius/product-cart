<?php

namespace app\activeRecords;

use Yii;

/**
 * This is the model class for table "c_cash_register".
 *
 * @property int $id
 * @property int $organization_id
 * @property string $name
 * @property string $client_code
 * @property string $payment_place
 * @property int $is_default
 * @property int $created_at
 */
class CCashRegister extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'c_cash_register';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['organization_id', 'name', 'client_code', 'created_at'], 'required'],
      [['organization_id', 'created_at'], 'integer'],
      [['name', 'client_code'], 'string', 'max' => 255],
      [['payment_place'], 'string', 'max' => 1000],
      [['is_default'], 'string', 'max' => 1],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'organization_id' => 'Organization ID',
      'name' => 'Name',
      'client_code' => 'Client Code',
      'payment_place' => 'Payment Place',
      'is_default' => 'Is Default',
      'created_at' => 'Created At',
    ];
  }

  /**
   * @return null|object|\yii\db\Connection
   */
  public static function getDb() {
    return Yii::$app->get('db2'); // new database
  }
}
