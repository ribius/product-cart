<?php

namespace app\activeRecords;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cart_item".
 *
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 * @property string $price
 * @property string $total_price
 * @property int $quantity
 * @property \yii\db\ActiveQuery $product
 */
class CartItem extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'cart_item';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['cart_id', 'product_id', 'quantity'], 'required'],
      [['cart_id', 'quantity'], 'integer'],
      [['price', 'total_price'], 'number'],
      [['product_id'], 'unique', 'targetAttribute' => ['product_id', 'cart_id']],
      [['product_id'], 'string', 'length' => [1,20]],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'cart_id' => 'ID корзины',
      'product_id' => 'ID товара',
      'price' => 'Цена',
      'total_price' => 'Цена всего',
      'quantity' => 'Кол-во',
    ];
  }

  /**
   * @param bool $insert
   * @return bool
   */
  public function beforeSave($insert)
  {
    if(parent::beforeSave($insert)) {
      $this->price = $this->product->price / 100;
      $this->total_price = $this->price * $this->quantity;
      return true;
    }

    return false;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCart()
  {
    return $this->hasOne(Cart::class, ['cart_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProduct()
  {
    return $this->hasOne(GProductReference::class, ['code' => 'product_id']);
  }

  /**
   * @return mixed
   */
  public function getProductname()
  {
    //return $this->product;
    return $this->product['product']['name'];
  }

  public function getProductfullname()
  {
    return $this->product['product']['full_name'];
  }

  public static function findByProductAndCart($product_id, $cart_id) {
    return self::findOne(['product_id' => $product_id, 'cart_id' => $cart_id]);
  }

  public static function findById($id)
  {
    return self::findOne(['id' => $id]);
  }

}
