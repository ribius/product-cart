<?php

namespace app\activeRecords;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "g_product".
 *
 * @property int $id
 * @property int $nomenclature_id
 * @property string $name
 * @property string $full_name
 * @property int $client_id
 * @property string $client_code
 * @property int $is_service
 * @property int $created_at
 */
class GProduct extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'g_product';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['nomenclature_id', 'client_id', 'created_at'], 'integer'],
      [['name', 'client_id', 'created_at'], 'required'],
      [['name', 'client_code'], 'string', 'max' => 255],
      [['full_name'], 'string', 'max' => 1000],
      [['is_service'], 'string', 'max' => 1],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nomenclature_id' => 'Nomenclature ID',
      'name' => 'Name',
      'full_name' => 'Full Name',
      'client_id' => 'Client ID',
      'client_code' => 'Client Code',
      'is_service' => 'Is Service',
      'created_at' => 'Created At',
    ];
  }

  /**
   * @return null|object|\yii\db\Connection
   */
  public static function getDb() {
    return Yii::$app->get('db2');
  }
}
