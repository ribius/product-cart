<?php

namespace app\models;

use yii\base\Model;
use app\activeRecords\Cart as CartActiveRecord;
use app\activeRecords\CartItem as CartItemActiveRecords;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use Yii;

class Cart extends Model
{
  /** @var $_ar CartActiveRecord */
  protected $_ar;

  public function __construct(CartActiveRecord $ar, array $config = [])
  {
    $this->_ar = $ar;
    parent::__construct($config);
  }

  public function getId()
  {
    return $this->_ar->id;
  }

  public function getStatus()
  {
    return $this->_ar->status;
  }

  public function getCreated()
  {
    return $this->_ar->created;
  }

  public function getChanged()
  {
    return $this->_ar->changed;
  }

  public function getItems()
  {
    $items = [];
    foreach ($this->_ar->items as $item) {
      /** @var $item \app\activeRecords\CartItem */
      $items[] = new CartItem($item);
    }
    return $items;
      //return $this->_ar->items;
  }

  public function getPaymentServiceId()
  {
    if(count($this->_ar->items) > 0) {
      return $this->_ar->items[0]['product']['payment_service_id'];
    }
    return null;
  }

  public function getAccessToken()
  {
    return (count($this->_ar->items) > 0) ? $this->_ar->items[0]['product']['user']['access_token'] : null;
  }

  public function getClientCode()
  {
    return (count($this->_ar->items) > 0) ? $this->_ar->items[0]['product']['cash_register']['client_code'] : null;
  }

  public function getTotalPrice()
  {
    return $this->_ar->getTotalPrice();
  }

  public static function create()
  {
    $ar = new CartActiveRecord();
    $ar->status = 0;
    $ar->created = time();
    $ar->changed = time();
    return ($ar->save()) ? new static($ar) : null;
  }

  public function flush()
  {
    $db = Yii::$app->getDb();
    try {
      $db->createCommand()
        ->delete(CartItemActiveRecords::tableName(), "cart_id = {$this->id}")
        ->execute();
      return new static($this->_ar);
    }catch (\Throwable $e) {
       Yii::error('Ошибка при очистки корзины: ' . implode(PHP_EOL, $e->getTrace()));
    }

    throw new ServerErrorHttpException('Ошибка при очистки корзины');
  }

  public static function findById($id)
  {
    if(($ar = CartActiveRecord::findOne($id)) != null) {
      return new static($ar);
    }else {
      throw new NotFoundHttpException('Корзина не найдена');
    }
  }
}