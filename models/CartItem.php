<?php

namespace app\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use app\activeRecords\CartItem as CartItemActiveRecord;
use yii\caching\Cache;

class CartItem extends Model
{
  /** @var $_ar \app\activeRecords\CartItem */
  protected $_ar;

  public function __construct(CartItemActiveRecord $ar, array $config = [])
  {
    $this->_ar = $ar;
    parent::__construct($config);
  }

  public function getId()
  {
    return $this->_ar->id;
  }

  public function getCartId()
  {
    return $this->_ar->cart_id;
  }

  public function getProductId()
  {
    return $this->_ar->product_id;
  }

  public function getQuantity()
  {
    return $this->_ar->quantity;
  }

  public function getPrice()
  {
    return $this->_ar->price;
  }

  public function getTotalPrice()
  {
    return $this->_ar->total_price;
  }

  public function getProduct()
  {
    $cache = \Yii::$app->cache;
    return $cache->getOrSet($this->_ar->product_id, function () {
      return $this->_ar->product;
    });
  }

  public function getProductName()
  {
    return ($this->product) ? $this->product['product']['name'] : null;
  }

  public function getProductFullName()
  {
    return ($this->product) ? $this->product['product']['full_name'] : null;
  }

  public function getProductProductId()
  {
    return ($this->product) ? $this->product['product_id'] : null;
  }

  public function create($cart_id, $product_id, $quantity)
  {
    $ar = new CartItemActiveRecord();
    $ar->cart_id = $cart_id;
    $ar->product_id = $product_id;
    $ar->quantity = $quantity;
    if($ar->save()) {
      return true;
    }
    if($ar->hasErrors()) {
      throw new InvalidArgumentException(implode(PHP_EOL, $ar->getFirstErrors()));
    }
    throw new ServerErrorHttpException('Ошибка сервера cart_item');
  }

  public function quantityIncrement()
  {
    ++$this->_ar->quantity;
    if($this->_ar->save()) {
      return true;
    }
    if($this->_ar->hasErrors()) {
      throw new InvalidArgumentException(implode(PHP_EOL, $this->_ar->getFirstErrors()));
    }
  }

  public function quantityDecrement()
  {
    --$this->_ar->quantity;
    if($this->_ar->save()) {
      return true;
    }
    if($this->_ar->hasErrors()) {
      throw new InvalidArgumentException(implode(PHP_EOL, $this->_ar->getFirstErrors()));
    }
  }

  public function delete()
  {
    return $this->_ar->delete();
  }
}